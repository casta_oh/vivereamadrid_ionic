'use strict';

angular.module('vivereamadridFilters', [])
    .filter('parseStringToDate', function() {
        return function(str1) {
            //yyyy-mm-dd
            var yr1   = parseInt(str1.substring(0,4));
            var mon1  = parseInt(str1.substring(6,7));
            var dt1   = parseInt(str1.substring(8,10));
            var date1 = new Date(yr1, mon1-1, dt1);
            return date1;

        };

    });