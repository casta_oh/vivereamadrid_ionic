'use strict';

/* Services */

var vivereamadridServices = angular.module('vivereamadridServices', ['ngResource']);

vivereamadridServices.factory('AllPostsService', function($resource) {
    return $resource('http://www.vivereamadrid.it/api/get_posts?page=:id&count=6');
    //return $resource('posts/allpost.json');
});

vivereamadridServices.factory('SearchPostsService', function($resource) {
    return $resource('http://www.vivereamadrid.it/api/get_posts?s=:cadena&page=:id&count=6');
    //return $resource('posts/search.json');
});

vivereamadridServices.factory('LastPostService', function($resource) {
    //return $resource('http://localhost:3000/api/lastpost');
    return $resource('http://www.vivereamadrid.it/api/get_recent_posts?count=1');
    //return $resource('posts/laspostmongo.js');
});


vivereamadridServices.factory('PostService', function($resource) {
    return $resource('http://www.vivereamadrid.it/api/get_post/?post_id=:id');
    //return $resource('posts/post.json');
});

vivereamadridServices.factory('PageService', function($resource) {
    return $resource('http://www.vivereamadrid.it/api/get_page/?page_id=:pageId&json=1');
    //return $resource('posts/aeropuertomadrid.json');
});

vivereamadridServices.factory('PostsByCategoryService', function($resource) {
    return $resource('http://www.vivereamadrid.it/api/get_category_posts/?category_id=:id&page=:pageId&count=6');
    //return $resource('posts/postbycategory.json');
});

vivereamadridServices.factory('MarkersMapService', function($resource) {
    //borro la llamada a wordpress porque es insostenible
    //return $resource('http://www.vivereamadrid.it/api/get_posts/?meta_key=google_maps_mostrar&meta_value=SI&count=20');
    return $resource('posts/markers.json');
});

vivereamadridServices.factory('FavoritePostService', function($ionicLoading) {
    //servicio para añadir o quitar favoritos de la local storage
    return ({addFavorite: addFavorite,
            deleteFavorite: deleteFavorite,
            isFavorite: isFavorite,
            getAll: getAll});

        function getAll() {

            var favoritesMap = window.localStorage.getItem("FAVORITE_POST");
            favoritesMap = JSON.parse(favoritesMap);
            return favoritesMap;
        }

        function addFavorite() {

            var fn = function(post) {

                $ionicLoading.show({ template: 'Aggiunto!', noBackdrop: true, duration: 1000 });
            
                console.log("aniadiendo el post a favoritos");
                var favoritesMap = window.localStorage.getItem("FAVORITE_POST");
                
                if (favoritesMap == undefined) {
                    console.log("inicializando favoritos");
                    favoritesMap = new Object();
                } else {
                    //como no es nulo, tengo que pasarlo de string a formato lista js
                    favoritesMap = JSON.parse(favoritesMap);
                }

                favoritesMap[post.id] = post;
                window.localStorage.setItem("FAVORITE_POST", JSON.stringify(favoritesMap));
            };
            return fn;

        }

        function deleteFavorite(post) {

            console.log("borrando el post: " + post.id);
            var favoritesMap = window.localStorage.getItem("FAVORITE_POST");

            if (favoritesMap == undefined) {
                console.log("no hay ningun favorito .No hacemos nada");
            } else {
                //como no es nulo, tengo que pasarlo de string a formato lista js
                favoritesMap = JSON.parse(favoritesMap);
            }
            //var index = favoritesMap.indexOf(post.id);
            //favoritesMap.splice(post.id, 1);
            delete favoritesMap[post.id];

            var t = favoritesMap[post.id];

            console.log("Numero de posts favoritos: " + favoritesMap.length);
            window.localStorage.setItem("FAVORITE_POST", JSON.stringify(favoritesMap));


        }

        function isFavorite(post) {
 
            var favoritesMap = window.localStorage.getItem("FAVORITE_POST");
            if (favoritesMap == undefined) {
                return false;
            } else {
                favoritesMap = JSON.parse(favoritesMap);
                console.log("Posts en favoritos: " + Object.keys(favoritesMap));
                var favi = favoritesMap[post.id];
                var isFavorite = favi != undefined;
                console.log("Es favorito: " + isFavorite);
                return isFavorite;
            
            }

        }
    
});

var push;

vivereamadridServices.factory('PushService', function($http) {

    return ({   init: init,
                unregister: unregister,
                register: register,
                getTokenGCMFromLocalStorage: getTokenGCMFromLocalStorage});

    function unregister() {

        if (navigator.userAgent.match(/Android/i) != null) {
            push.unregister(function(data) {
                console.log('success unregister' + data);
                successUnRegisterHandlerGSM();

            }, function() {
                console.log('error');
            });
        }
    }

    function successUnRegisterHandlerGSM() {
        console.log("successUnRegisterHandlerGSM");
        var tokenGCM = getTokenGCMFromLocalStorage();

        window.localStorage.removeItem("PUSH_REGISTRATION_ID");

        $http({
            method: 'POST',
            url: 'http://www.vivereamadrid.it/gcm_server_php/unregister.php',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {regId: tokenGCM}
        }).success(function (response) {
            console.log("nos hemos borrado!!!" + JSON.stringify(data));
        });


    }

    function register() {

        if (navigator.userAgent.match(/Android/i) != null) {
            push = PushNotification.init({
              android: {
                  senderID: "321152883046",
                  icon: "ic_stat_name"
              },
              browser: {
                  pushServiceURL: 'http://push.api.phonegap.com/v1/push'
              },
              ios: {
                  alert: "true",
                  badge: "true",
                  sound: "true"
              },
              windows: {}
            });

            push.on('registration', function(data) {
              console.log('Tras el registro: ' + data.registrationId);
              //metemos en localstorage el registrationId
              registerOnVivereamadridServer(data.registrationId);
              setTokenGCMFromLocalStorage(data.registrationId);
            });

            push.on('notification', function(data) {
              
            });

        }
        

    }

    function registerOnVivereamadridServer(registrationId) {
        console.log("registrando el id: "+ registrationId);
        
        $http({
            method: 'POST',
            url: 'http://www.vivereamadrid.it/gcm_server_php/register.php',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {regId: registrationId}

        }).success(function (response) {
            console.log("nos hemos registrado!!!" + JSON.stringify(data));
        });
        }

    function getTokenGCMFromLocalStorage() {

        var tokenGCM = window.localStorage.getItem("PUSH_REGISTRATION_ID");
        if (tokenGCM != null && tokenGCM != '') {
            console.log("obteniendo el tokenGSM: " + window.localStorage.getItem("PUSH_REGISTRATION_ID"));
        } else {
            console.log("No hay tokenGCM en la sesion");
        }
        return tokenGCM;

    }

    function setTokenGCMFromLocalStorage(registrationId) {
        window.localStorage.setItem("PUSH_REGISTRATION_ID", registrationId);

    }

    function init() {

        var semaforo = window.localStorage.getItem("SEMAFORO_PUSH");
        console.log('Inicializando app, comprobar si ya hemos intentado llamar a push o no. semaforo: ' + semaforo);
        if (semaforo == null || semaforo == '') {

            var tokenGCM = getTokenGCMFromLocalStorage();
            
            if (tokenGCM == null || tokenGCM == '') {
                register();
                //cerramos el semaforo para que no se vuelva a inicializar al cargar de nuevo la app
                window.localStorage.setItem("SEMAFORO_PUSH", true);
            }
        }
        
    }


});

vivereamadridServices.factory('RateAppService', function() {
    
    return ({init: init,
            promptForRating: promptForRating});

        function promptForRating() {
            AppRate.promptForRating();
        }


        function init() {
            if (navigator.userAgent.match(/Android/i) != null) {
                AppRate.preferences = {
                  openStoreInApp: false,
                  displayAppName: 'Vota the app',
                  usesUntilPrompt: 3,
                  promptAgainForEachNewVersion: false,
                  storeAppURL: {
                    android: 'market://details?id=com.notjustjava.vivereamadrid',
                  },
                  customLocale: {
                    title: "Vota la app",
                    message: "Hai trovato questa app utile e divertente? Per favore votala!",
                    cancelButtonLabel: "No, Grazie",
                    laterButtonLabel: "Ricordamelo più tardi",
                    rateButtonLabel: "Voto subito!"
                  }
                };
                AppRate.preferences.useLanguage = 'it';
                AppRate.promptForRating(false);
            }
        }

    
});

vivereamadridServices.factory('ShareService', function() {
    
    return ({sharePost: sharePost,
             sharePage: sharePage,
             shareApp: shareApp});

        function sharePost() {

            var fn = function(post) {
                window.plugins.socialsharing.share('Leggi questo post: ', null, null, post.url);
            };
            return fn;

            
        };

        function sharePage() {

            var fn = function(page) {
                window.plugins.socialsharing.share('Leggi questo post: ', null, null, page.url);
            };
            return fn;

            
        };

        function shareApp() {

            window.plugins.socialsharing.share('Ti raccomando la app di Vivere a Madrid: ', null, null, 'https://play.google.com/store/apps/details?id=com.notjustjava.vivereamadrid');
            
        }
    
});