angular.module('vivereamadrid', [
  'ionic', 
  'starter.controllers',
  'vivereamadridServices',
  'vivereamadridcontrollers',
  'vivereamadridDirectives',
  'vivereamadridFilters',
  'ngCordova'])
.run(function($ionicPlatform, PushService, RateAppService) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    PushService.init();

    RateAppService.init();


  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.singlepost', {
    url: '/singlepost/:postId',
    views: {
      'menuContent': {
        templateUrl: 'templates/singlepost.html',
        controller: 'SinglePostCtrl'
      }
    }
  })
  .state('app.categories', {
    url: '/categories',
    views: {
      'menuContent': {
        templateUrl: 'templates/categories/list_categories.html'
        //,controller: 'SinglePostCtrl'
      }
    }
  })
  .state('app.favorites', {
    url: '/favorites',
    views: {
      'menuContent': {
        templateUrl: 'templates/favorites.html',
        controller: 'FavoritesCtrl'
      }
    }
  })
  .state('app.posts_by_category', {
    url: '/posts_by_category/:categoryId/:pageId',
    views: {
      'menuContent': {
        templateUrl: 'templates/categories/posts_by_category.html',
        controller: 'PostByCategoryCtrl'
      }
    }
  })
  .state('app.metromadrid', {
    url: '/metromadrid',
    views: {
      'menuContent': {
        templateUrl: 'templates/mapmetro.html'
      }
    }
  })
  .state('app.allposts', {
    url: '/allposts',
    views: {
      'menuContent': {
        templateUrl: 'templates/allposts.html',
        controller: 'AllPostsCtrl'

      }
    }
  })
  .state('app.markers', {
    url: '/markers',
    views: {
      'menuContent': {
        templateUrl: 'templates/markers.html',
        controller: 'MarkersCtrl'

      }
    }
  })
  .state('app.singlepage', {
    url: '/singlepage/:pageId',
    views: {
      'menuContent': {
        templateUrl: 'templates/singlepage.html',
        controller: 'SinglePageCtrl'

      }
    }
  })
  .state('app.alltransports', {
    url: '/alltransports',
    views: {
      'menuContent': {
        templateUrl: 'templates/transports/alltransports.html'

      }
    }
  })
  .state('app.configuration', {
    url: '/configuration',
    views: {
      'menuContent': {
        templateUrl: 'templates/configuration.html',
        controller: 'ConfigurationCtrl'
      }
    }
  })
  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/allposts');
});

