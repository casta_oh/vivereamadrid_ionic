angular.module('vivereamadridcontrollers', [])

.controller('MainCtrl', ['$scope', '$location', 'LastPostService', 
    function($scope, $location, LastPostService) {
        
        LastPostService.get({}, function(data) {
              console.log(data);
              $scope.post = data.post;

            }, function (error) {
              console.log("Errores obteniendo el último artículo " + error);
            }

          )            

    }])
.controller('SinglePostCtrl', ['$scope', 'PostService', 'FavoritePostService', '$state', '$ionicLoading', 'ShareService', '$http', 
    function($scope, PostService, FavoritePostService, $state, $ionicLoading, ShareService, $http) {

      $ionicLoading.show({
            template: 'Loading...'
          });

        console.log('Obteniendo el post id: ' + $state.params.postId);
        PostService.get( {id: $state.params.postId }, 
            function(data) {
              $scope.post = data.post;
              
              if (data.previous_id != null) {
                var previous = {};
                previous.id = data.previous_id;
                previous.title = data.previous_title;
                $scope.previous = previous;
              }

              if (data.next_id != null) {
                var next = {};
                next.id = data.next_id;
                next.title = data.next_title;
                $scope.next = next;
              }

              $scope.isFavorite = FavoritePostService.isFavorite(data.post);

              $ionicLoading.hide();
              
            }, function(error) {
                console.log("Errores obteniendo el artículo " + error);

        });

        $scope.addFavorite = FavoritePostService.addFavorite();
        
        $scope.sharePost = ShareService.sharePost();
        

    }])
.controller('SinglePageCtrl', ['$scope', 'PageService', 'FavoritePostService', '$state', '$ionicLoading', 'ShareService',  
    function($scope, PageService, FavoritePostService, $state, $ionicLoading, ShareService) {
        
      $ionicLoading.show({
            template: 'Loading...'
          });

        console.log('Obteniendo el page id: ' + $state.params.pageId);
        PageService.get( {pageId: $state.params.pageId }, 
            function(data) {
              $scope.page = data.page;
              
              $ionicLoading.hide();
              
            }, function(error) {
                console.log("Errores obteniendo la pagina " + error);

        });
        
        $scope.sharePage = ShareService.sharePage();
        

    }])
.controller('ConfigurationCtrl', ['$scope', 'PushService',
    function($scope, PushService) {
        
        var tokenGCM = PushService.getTokenGCMFromLocalStorage();
        console.log('tokenGCM: ' + tokenGCM);
        if (tokenGCM != null && tokenGCM != '') {
          $scope.pushactive = true;
        } else {
          $scope.pushactive = false;
        }

        $scope.onclick = function() {
          console.log("Activando PUSH notifications " + $scope.pushactive);
          if ($scope.pushactive == false) {
            //borrando registro de PUSH
            PushService.unregister();
          } else {
            PushService.register();
          }


        }

    }])
.controller('FavoritesCtrl', ['$scope', 'FavoritePostService', 'ShareService' , 
    function($scope, FavoritePostService, ShareService) {
        
        
         $scope.data = {
            showDelete: false
          };
          
          $scope.onItemDelete = function(item) {
            FavoritePostService.deleteFavorite(item);
            initDatos();
          };
  

          $scope.onItemShared = ShareService.sharePost();


        function initDatos() {
          $scope.favorites = FavoritePostService.getAll();    
          $scope.numPosts = 0;
          if ($scope.favorites != null) {
              $scope.numPosts = Object.keys($scope.favorites).length;
          }

          
        }

        initDatos();


    }])
.controller('SearchCtrl', ['$scope', 'SearchPostsService', '$ionicLoading', 'FavoritePostService', 'ShareService',
    function($scope, SearchPostsService, $ionicLoading, FavoritePostService, ShareService) {


        $scope.key = '';
        $scope.currentPage = 0;
        $scope.totalPages = 0;

        //implementa la busqueda
        $scope.search = function () {

            if ($scope.key != '' && $scope.key != undefined) {

                $scope.currentPage = 1;
                $scope.totalPages = 0;

                $ionicLoading.show({
                    template: 'Loading...'
                });

                SearchPostsService.get( {cadena: $scope.key, page: $scope.currentPage }, 
                  function (data) {

                      console.log("Tratando la respuesta");
                      $scope.posts = data.posts;
                      $scope.totalPages = data.pages;
                      $ionicLoading.hide();

                });
                
            };
            
        };

        //implementa el infinite scrolling
        $scope.loadMoreData = function(){

          $scope.currentPage += 1;


          
          SearchPostsService.get( {cadena: $scope.key, page: $scope.currentPage }, 
            function (data) {

                $scope.posts = $scope.posts.concat(data.posts);
                $scope.totalPages = data.pages;
                $scope.$broadcast('scroll.infiniteScrollComplete');

          });

        };

        $scope.sharePost = ShareService.sharePost();

        $scope.addFavorite = FavoritePostService.addFavorite();

        $scope.moreDataCanBeLoaded = function(){
          return $scope.totalPages > $scope.currentPage;
        };



    }])

.controller('AllPostsCtrl', ['$scope', 'AllPostsService', '$state', '$ionicLoading', 'FavoritePostService', 'ShareService',   
    function($scope, AllPostsService, $state, $ionicLoading, FavoritePostService, ShareService) {
        
        console.log('Obteniendo todos los posts ');

        $scope.page = 1;
        $scope.totalPages = 0;

        $scope.doRefresh = function() {
            
          $ionicLoading.show({
              template: 'Loading...'
          });

          AllPostsService.get( {id: $scope.page }, 
            function (data) {

                console.log("Tratando la respuesta de todos los posts");
                $scope.posts = data.posts;
                $scope.totalPages = data.pages;
                $ionicLoading.hide();

          });

        }

        $scope.loadMoreData = function(){

          $scope.page += 1;

          AllPostsService.get( {id: $scope.page }, 
            function (data) {

                console.log("Tratando la respuesta de todos los posts");
                $scope.posts = $scope.posts.concat(data.posts);
                $scope.totalPages = data.pages;
                $scope.$broadcast('scroll.infiniteScrollComplete');

          });
        
        };

        $scope.moreDataCanBeLoaded = function(){
          return $scope.totalPages > $scope.page;
        };

        $scope.addFavorite = FavoritePostService.addFavorite();

        $scope.sharePost = ShareService.sharePost();

        $scope.doRefresh();



    }])

.controller('PostByCategoryCtrl', ['$scope', 'PostsByCategoryService', '$state', '$ionicLoading', 'FavoritePostService', 'ShareService',  
    function($scope, PostsByCategoryService, $state, $ionicLoading, FavoritePostService, ShareService) {
        
        console.log('Obteniendo los posts de la categoria: ' + $state.params.categoryId);
        console.log('Numero de pagina: ' + $state.params.pageId);

        $scope.page = 1;
        $scope.totalPages = 1;

        $scope.doRefresh = function() {
            
          $ionicLoading.show({
              template: 'Loading...'
          });

          PostsByCategoryService.get( {id: $state.params.categoryId, pageId: $scope.page }, 
            function (data) {

                console.log("Tratando la respuesta de los posts por categoria");
                $scope.posts = data.posts;
                $scope.totalPages = data.pages;
                $ionicLoading.hide();

          });

        }

        $scope.loadMoreData = function(){

          $scope.page += 1;

          
          PostsByCategoryService.get( {id: $state.params.categoryId, pageId: $scope.page }, 
            function (data) {

                console.log("Tratando la siguiente pagina respuesta de los posts por categoria");
                $scope.posts = $scope.posts.concat(data.posts);
                $scope.totalPages = data.pages;
                $scope.$broadcast('scroll.infiniteScrollComplete');

          });

        };

        $scope.moreDataCanBeLoaded = function(){
          return $scope.totalPages > $scope.page;
        };

        $scope.addFavorite = FavoritePostService.addFavorite();

        $scope.sharePost = ShareService.sharePost();
        
        $scope.doRefresh();

    }])
.controller('MarkersCtrl', function($scope, $ionicLoading, $cordovaGeolocation, MarkersMapService, $ionicPopup) {
 
    //creamos el mapa y lo centramos en sol
    var latLng = new google.maps.LatLng(
        40.416969, 
        -3.703528);
   
    var mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      
    var ttt = document.getElementById("map");
    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
 
    $ionicLoading.show({
        template: 'Loading...'
    });

    MarkersMapService.get( {}, 
      function (postsMarkers) {

          console.log("Tratando la respuesta de todos los markers");
          $ionicLoading.hide();

          $scope.auxM = [];
          for (var i = 0; i < postsMarkers.posts.length; i++) {
              
              var _post = postsMarkers.posts[i];

              if (_post.custom_fields.google_maps_latitude != null &&
                  _post.custom_fields.google_maps_longitude != null &&
                  _post.custom_fields.google_maps_direccion != null &&
                  _post.thumbnail != null ) {

                  //inicializamos la posicion de nuestro marker
                  var latLng = new google.maps.LatLng(
                    postsMarkers.posts[i].custom_fields.google_maps_latitude[0], 
                    postsMarkers.posts[i].custom_fields.google_maps_longitude[0]);

                  var marker = new google.maps.Marker({
                      map: $scope.map,
                      animation: google.maps.Animation.DROP,
                      position: latLng
                  }); 

                  addInfoWindow(marker, _post);

              } else {
                  console.log("Se silencia un articulo mal configurado: " + _post.url);
              }


          }


    });

    function addInfoWindow(marker, post) {

        var title = "<p><b>" + post.title + "</b></p>";
        var message =
                    "<a href=\"#/app/singlepost/" + post.id + "\">" + 
                    "<p class=\"text-center\"><img style=\" border-radius: 50%;width: 70px;\" src=\"" + post.thumbnail + "\"/></p>" + 
                    title + "</a>";

        var infoWindow = new google.maps.InfoWindow({
            content: message
        });
   
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open($scope.map, marker);
        });
   
    }

    $scope.centerOnMe= function(){
      
      //comprobado si GPS activado
      cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
          console.log("GPS location is " + enabled);
          if (!enabled) {
              //mostramos el modal para saber si quiere activar el gps o no
              showConfirm();
          } else {
            goCenter();
          }
            
      }, function(error){
          alert("The following error occurred: "+error);
      });

     // A confirm dialog
   function showConfirm() {
       
       var confirmPopup = $ionicPopup.confirm({
         title: 'Il tuo GPS non è attivo',
         template: 'Clicca su Accettare per configurare il GPS'
       });
       confirmPopup.then(function(res) {
         if(res) {
           cordova.plugins.diagnostic.switchToLocationSettings();
         }
       });
       
   };

   function goCenter() {

        // with this function you can get the user’s current position
      // we use this plugin: https://github.com/apache/cordova-plugin-geolocation/
      navigator.geolocation.getCurrentPosition(function(position) {
        
        $ionicLoading.show({
          template: 'Loading...'
        });

        var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        $scope.current_position = {lat: pos.G,lng: pos.K};
        $scope.my_location = pos.G+", "+pos.K;
        $scope.map.setCenter(pos);
        
        //inicializamos la posicion de nuestro marker
        var latLngCentered = new google.maps.LatLng(
          position.coords.latitude, 
          position.coords.longitude);


        var marker = new google.maps.Marker({
                      map: $scope.map,
                      animation: google.maps.Animation.DROP,
                      position: latLngCentered,
                      icon: 'img/man32.png'
                  }); 

        marker.addListener('click', function() {
            infoWindow.open($scope.map, marker);
          });

        marker["id"] = $scope.auxM.length + 1;
        $scope.auxM.push(marker);  

        $ionicLoading.hide();
      });
    
    };


    };
});

