var vivereamadridDirectives = angular.module('vivereamadridDirectives', []);


vivereamadridDirectives.directive('cardPost', function() {
    return {
    	restrict: 'E',
    	templateUrl: 'templates/directives/card-post.html'
  };
});
